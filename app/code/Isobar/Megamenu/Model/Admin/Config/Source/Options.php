<?php
/**
 * Created by PhpStorm.
 * User: ubuntu
 * Date: 12/06/2017
 * Time: 15:04
 */

namespace Isobar\Megamenu\Model\Admin\Config\Source;


class Options implements \Magento\Framework\Data\OptionSourceInterface
{
    /**
     * @var array
     */
    public $data = [['value' => 0, 'label' => 'Root']];

    /**
     * @var null|array
     */
    protected $options;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @var \Isobar\Megamenu\Api\MegamenuRepositoryInterface
     */
    protected $megaMenuRepository;

    /**
     * @var \Magento\Framework\Api\SortOrder
     */
    protected $sortOrder;

    /**
     * @var \Magento\Framework\Api\FilterBuilder
     */
    protected $filterBuilder;

    /**
     * @var \Magento\Framework\Api\Search\FilterGroupBuilder
     */
    protected $filterGroupBuilder;

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * Options constructor.
     * @param \Isobar\Megamenu\Api\MegamenuRepositoryInterface $megaMenuRepository
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Framework\Api\SortOrder $sortOrder
     * @param \Magento\Framework\Api\FilterBuilder $filterBuilder
     * @param \Magento\Framework\Api\Search\FilterGroupBuilder $filterGroupBuilder
     * @param \Magento\Framework\App\Request\Http $request
     */
    public function __construct(
        \Isobar\Megamenu\Api\MegamenuRepositoryInterface $megaMenuRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Api\SortOrder $sortOrder,
        \Magento\Framework\Api\FilterBuilder $filterBuilder,
        \Magento\Framework\Api\Search\FilterGroupBuilder $filterGroupBuilder,
        \Magento\Framework\App\Request\Http $request
    ) {
        $this->request = $request;
        $this->megaMenuRepository = $megaMenuRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sortOrder = $sortOrder;
        $this->filterBuilder = $filterBuilder;
        $this->filterGroupBuilder = $filterGroupBuilder;
    }

    /**
     * @return array|null
     */
    public function toOptionArray()
    {
        $filterGroups = [];
        if ($menuId = $this->request->getParam('id')) {
            $filter = $this->filterBuilder
                ->create()
                ->setField('id')
                ->setValue($menuId)
                ->setConditionType('neq');

            $filterGroup = $this->filterGroupBuilder
                ->addFilter($filter)
                ->create();
            $filterGroups = [$filterGroup];
        }

        $searchCriteria = $this->searchCriteriaBuilder
            ->setFilterGroups($filterGroups)
            ->create();
        $sortOrder = $this->sortOrder->setField('sort')->setDirection(\Magento\Framework\Api\SortOrder::SORT_ASC);
        $searchCriteria->setSortOrders([$sortOrder]);
        $result = $this->megaMenuRepository->getList($searchCriteria);
        $items = $result->getItems();
        $treeData = $this->getMenuItemTreeData($items);
        $this->buildHierachySelectOptions(0, $treeData);
        return $this->data;
    }

    /**
     * Get megamenu item to tree data
     * @param $items
     * @return array
     */
    public function getMenuItemTreeData($items)
    {
        $menuItemData = array(
            'items' => array(),
            'parents' => array()
        );
        foreach ($items as $menuItem) {
            $menuItemData ['items'] [$menuItem->getId()] = $menuItem;
            $menuItemData ['parents'] [$menuItem->getParentId()] [] = $menuItem->getId();
        }
        return $menuItemData;
    }

    /**
     * Prepare options data
     * @param $parentId
     * @param $treeData
     * @param int $level
     */
    public function buildHierachySelectOptions($parentId, $treeData, $level = -1)
    {
        $data = [];
        $level ++;
        if (isset($treeData['parents'][$parentId])) {
            foreach ($treeData['parents'][$parentId] as $itemId) {
                $this->data[] = ['value' => $treeData['items'][$itemId]->getId(), 'label' => $this->_getIndent($level). ' ' . $treeData['items'][$itemId]->getTitle()];
                $this->buildHierachySelectOptions($itemId, $treeData, $level);
            }
        }
    }

    /**
     * Get indent for menu tree
     * @param $level
     * @return string
     */
    protected function _getIndent($level)
    {
        $indent = '----';
        for ($i = 1; $i <= $level; $i++) {
            $indent .='----';
        }
        return $indent;
    }
}
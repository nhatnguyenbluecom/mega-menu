<?php

namespace Isobar\Megamenu\Api\Data;


interface MegamenuInterface
{
    /**
     * Constants defined for keys of data array
     */
    const TITLE         = 'title';
    const LINK          = 'link';
    const DESCRIPTION   = 'description';
    const EXTEND_HTML   = 'extend_html';
    const STATUS        = 'status';
    const SORT          = 'sort';
    const CREATED_AT       = 'created';

    /**
     * Get megamenu id
     * @return int|null
     */
    public function getId();

    /**
     * Set megamenu id
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * Get megamenu title
     * @return string|null
     */
    public function getTitle();

    /**
     * Set megamenu title
     * @param string $title
     * @return $this
     */
    public function setTitle($title);

    /**
     * Set megamenu link
     * @param string $link
     * @return $this
     */
    public function setLink($link);

    /**
     * Get megamenu link
     * @return string|null
     */
    public function getLink();

    /**
     * Set megamenu description
     * @param string $description
     * @return $this
     */
    public function setDescription($description);

    /**
     * Get megamenu description
     * @return string|null
     */
    public function getDescription();

    /**
     * Set megamenu extend_html
     * @param string $description
     * @return $this
     */
    public function setExtendHtml($extendHtml);

    /**
     * Get megamenu extend_html
     * @return string|null
     */
    public function getExtendHtml();

    /**
     * Set megamenu status
     * @param int $status
     * @return $this
     */
    public function setStatus($status);

    /**
     * Get megamenu status
     * @return int|null
     */
    public function getStatus();

    /**
     * Set megamenu sort
     * @param int $sort
     * @return $this
     */
    public function setSort($sort);

    /**
     * Get megamenu sort
     * @return int|null
     */
    public function getSort();

    /**
     * Get megamenu created date
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set megamenu created date
     *
     * @param string $timeStamp
     * @return $this
     */
    public function setCreatedAt($timeStamp);
}